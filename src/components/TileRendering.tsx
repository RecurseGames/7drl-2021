import { useSelector } from 'react-redux';
import Tile from '../model/Tile';
import { selectGame } from "../store";

type TileRenderingProps = {
    x: number,
    y: number
}

function getGlyph(tile: Tile | null) {
    if (!tile) {
        return "?";
    } else if (tile.occupant) {
        return tile.occupant.getGlyph();
    } else if (tile.containsDoor) {
        return "O";
    } else if (tile.containsWall()) {
        return "#";
    } else if (tile.containsVoid()) {
        return "\u00A0";
    } else {
        return ".";
    }
}

export default function TileRendering({ x, y }: TileRenderingProps) {
    const { dungeon } = useSelector(selectGame);
    const tile = dungeon && dungeon.tryGetTile(x, y);

    return <div style={{width: "24px", height: "24px", fontFamily: "monospace", fontSize: "24px"}}>
        {
            getGlyph(tile)
        }
    </div>;
}
