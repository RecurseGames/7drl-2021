import { useSelector } from 'react-redux';
import { selectGame } from "../store";
import TileRendering from './TileRendering';

export default function DungeonRendering() {
    const { dungeon } = useSelector(selectGame);
    if (!dungeon) {
        return <p>Dungeon not loaded.</p>;
    }

    const rowIndices = dungeon.getTileRowIndices();
    const columnIndices = dungeon.getTileColumnIndices();
    return <div>
        {
            rowIndices.map(y => (
                <div style={{display: "flex", flexDirection: "row"}} key={y}>
                    {
                        columnIndices.map(x => <TileRendering key={x} x={x} y={y} />)
                    }
                </div>
            ))
        }
    </div>;
}
