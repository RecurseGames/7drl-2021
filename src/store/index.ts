import { createStore, combineReducers } from 'redux';
import gameReducer from './reducers/gameReducer'

const rootReducer = combineReducers({
  game: gameReducer,
})

export type RootState = ReturnType<typeof rootReducer>

export const selectGame = (state: RootState) => state.game;

export default function configureStore(initialState = {}) {
    const store = createStore(rootReducer, initialState);
    return store;
}