import DungeonState from "../../model/DungeonState";

interface State {
    dungeon: DungeonState | null
}

const UPDATE_DUNGEON = "UPDATE_DUNGEON";

interface UpdateDungeonAction {
    type: typeof UPDATE_DUNGEON
    payload: DungeonState
}

type ActionTypes = UpdateDungeonAction;

export function updateDungeon(dungeon: DungeonState): ActionTypes {
    return {
        type: UPDATE_DUNGEON,
        payload: dungeon
    }
}

export default function reducer(state: State = { dungeon: null }, action: ActionTypes): State  {
    switch (action.type) {
        case UPDATE_DUNGEON:
            return {
                ...state,
                dungeon: action.payload
            };
        default:
            return state
    }
}
