export default class Point {
    x: number
    y: number

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    add(x: number, y: number): Point {
        return new Point(this.x + x, this.y + y);
    }

    getNeighbours(): Point[] {
        return [
            this.add(1, -1),
            this.add(1, 0),
            this.add(1, 1),
            this.add(0, -1),
            this.add(0, 1),
            this.add(-1, -1),
            this.add(-1, 0),
            this.add(-1, 1),
        ];
    }
}
