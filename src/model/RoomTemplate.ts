import Point from './Point';
import Rng from './Rng';

export default class RoomTemplate {
    anchors: Point[];
    _tiles: Point[];

    constructor(template: string) {
        this._tiles = [];
        this.anchors = [];

        const rows = template.split(/\r?\n/).slice(1);
        for (let y = 0; y < rows.length; y++) {
            const row = rows[y];
            for (let x = 0; x < row.length; x++) {
                const tile = row[x];
                if (tile === 'X') {
                    this._tiles.push(new Point(x, y));
                } else if (tile === 'O') {
                    this.anchors.push(new Point(x, y));
                } else if (tile !== ' ') {
                    throw new Error(`Unexpected room template character: ${tile}`);
                }
            }
        }
    }

    getTiles(doorLocation: Point, seed: number) : Point[] {
        const rng = new Rng(seed);
        const anchor = this.anchors[rng.int(0, this.anchors.length)];
        const flipX = rng.bool();
        const flipY = rng.bool();
        const scaleX = flipX ? -1 : 1;
        const scaleY = flipY ? -1 : 1;

        return this._tiles.map(tile => new Point(
            scaleX * (tile.x - anchor.x) + doorLocation.x,
            scaleY * (tile.y- anchor.y) + doorLocation.y
        ));
    }

    static createList(templates: string[]) : RoomTemplate[] {
        return templates.map(template => new RoomTemplate(template));
    }
}
