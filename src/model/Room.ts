import { getNeighbourList } from "../processes/getNeighbours";
import Point from "./Point";

export default class Room {
    tiles: Point[]

    constructor(tiles: Point[]) {
        this.tiles = tiles;
    }

    getNeighbours(): Set<Point> {
        return getNeighbourList(this.tiles);
    }
}
