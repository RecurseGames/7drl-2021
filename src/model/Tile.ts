import Room from "./Room";
import Thing from "./Thing";

export default class Tile {
    room: Room | null;
    containsDoor: boolean;
    adjacentRooms: Room[];
    occupant: Thing | null;
    
    constructor(
        room: Room | null = null,
        containsDoor: boolean = false,
        adjacentRooms: Room[] = new Array<Room>(0),
        occupant: Thing | null = null,
    ) {
        this.occupant = occupant;
        this.room = room;
        this.containsDoor = containsDoor;
        this.adjacentRooms = adjacentRooms;
    }

    containsFloor(): boolean {
        return !!this.room || (this.containsDoor && !!this.adjacentRooms);
    }

    containsVoid(): boolean {
        return !this.room && !this.adjacentRooms.length;
    }

    containsWall(): boolean {
        return !this.room && !!this.adjacentRooms.length && !this.containsDoor;
    }

    setContainsDoor(containsDoor: boolean): Tile {
        return new Tile(this.room, containsDoor, this.adjacentRooms, this.occupant);
    }

    setOccupant(occupant: Thing | null): Tile {
        return new Tile(this.room, this.containsDoor, this.adjacentRooms, occupant);
    }

    addAdjacentRoom(room: Room): Tile {
        if (this.adjacentRooms.includes(room)) {
            return this;
        } else {
            return new Tile(this.room, this.containsDoor, this.adjacentRooms.concat(room), this.occupant);
        }
    }

    setRoom(room: Room): Tile {
        if (!this.containsVoid()) {
            throw new Error('Can only place room in void.');
        }
        return new Tile(room, this.containsDoor, this.adjacentRooms, this.occupant);
    }
}


