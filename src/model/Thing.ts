import Point from "./Point";

export default class Thing {
    isPlayer: boolean = false
    position: Point | null = null

    constructor(values: Partial<Thing>) {
        Object.assign(this, values);
    }
    
    getGlyph() {
        return this.isPlayer ? "@" : "!";
    }
}
