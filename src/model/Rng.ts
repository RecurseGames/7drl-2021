import Rand from "rand-seed";

export default class Rng {
    _impl: Rand

    constructor(seed: number) {
        this._impl = new Rand(seed.toString());
    }

    select<T>(items: Array<T>): T {
        const index = this.int(0, items.length);
        const item = items[index];
        return item;
    }

    choose<T>(items: Array<T>): T {
        const index = this.int(0, items.length);
        const item = items[index];
        items.splice(index, 1);
        return item;
    }

    bool(): boolean {
        return this._impl.next() <= 0.5;
    }

    next(): number {
        return this._impl.next()
    }

    int(min: number, max: number): number {
        if (max <= min) {
            throw new Error("Max must be above minimum");
        }
        const range = max - min;
        return Math.floor(this._impl.next() * range) + min;
    }
}