import Tile from "./Tile";
import Room from "./Room";
import Thing from "./Thing";
import Point from "./Point";

const DUNGEON_SIZE = 15;

export default class DungeonState {
    _tiles: Tile[]

    constructor(tiles: Tile[] | null = null) {
        if (tiles) {
            this._tiles = tiles;
        } else {
            this._tiles = new Array<Tile>(DUNGEON_SIZE * DUNGEON_SIZE);
            for (let i = 0; i < this._tiles.length; i++) {
                this._tiles[i] = new Tile();
            }
        }
    }

    _getTileIndex(x: number, y: number) {
        if (x < 0 || y < 0 || x >= DUNGEON_SIZE || y >= DUNGEON_SIZE) {
            return null;
        } else {
            return x * DUNGEON_SIZE + y;
        }
    }

    getTileRowIndices(): number[] {
        const indices = new Array(DUNGEON_SIZE);
        for (let i = 0; i < DUNGEON_SIZE; i++) {
            indices[i] = i;
        }
        return indices;
    }

    getTileColumnIndices(): number[] {
        const indices = new Array(DUNGEON_SIZE);
        for (let i = 0; i < DUNGEON_SIZE; i++) {
            indices[i] = i;
        }
        return indices;
    }

    copy() {
        return new DungeonState([...this._tiles]);
    }

    tryMoveThing(thing: Thing, point: Point): boolean {
        const tile = this.tryGetTile(point.x, point.y);
        if (tile && tile.containsFloor() && !tile.occupant) {
            thing.position && this.tryUpdateTile(thing.position.x, thing.position.y, tile => tile.setOccupant(null));
            this.updateTile(point.x, point.y, tile => tile.setOccupant(thing));
        }
        return false;
    }

    tryPlaceRoom(room: Room): boolean {
        if (room.tiles.some(({x, y}) => x === 0 || y === 0 || x === DUNGEON_SIZE - 1 || y === DUNGEON_SIZE - 1)) {
            return false;
        } else if (room.tiles.map(({x, y}) => this.tryGetTile(x, y)).every(tile => tile && tile.containsVoid())) {
            room.tiles.forEach(({x, y}) => this.updateTile(x, y, tile => tile.setRoom(room)));
            room.getNeighbours().forEach(({x, y}) => this.tryUpdateTile(x, y, tile => tile.addAdjacentRoom(room)));
            return true;
        } else {
            return false;
        }
    }

    tryGetTile(x: number, y: number): Tile | null {
        const index = this._getTileIndex(x, y);
        const tile = index === null ? null : this._tiles[index];
        return tile;
    }

    tryUpdateTile(x: number, y: number, update: (tile: Tile) => Tile): boolean {
        const index = this._getTileIndex(x, y);
        if (index == null) {
            return false;
        } else {
            const tile = this._tiles[index];
            const updatedTile = update(tile);
            this._tiles[index] = updatedTile;
            return true;
        }
    }

    updateTile(x: number, y: number, update: (tile: Tile) => Tile) {
        if (!this.tryUpdateTile(x, y, update)) {
            throw Error('No tile exists to update.');
        }
    }
}
