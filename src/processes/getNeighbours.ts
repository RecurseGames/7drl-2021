import Point from "../model/Point";

export function getNeighbourList(points: Point[]): Set<Point> {
    const neighbours = new Set<Point>();
    points.forEach(point => point.getNeighbours().forEach(neighbour => neighbours.add(neighbour)));
    return neighbours;
}