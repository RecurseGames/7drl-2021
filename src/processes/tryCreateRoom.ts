import DungeonState from '../model/DungeonState';
import Room from '../model/Room';
import RoomTemplate from '../model/RoomTemplate';
import Rng from '../model/Rng';
import Point from '../model/Point';

const ROOM_TEMPLATES = RoomTemplate.createList([
`
OO
XXO
XXO
`, `
 O
 XO
XXO
X
`, `
 O
 X
XXXO
 X
`, `
 OO
 XX
XXXXO
XXXXO
 XX
`, `
OOOO
XXXXO
XXXXO
`, `
 OO
 XXO
XXXO
XX
`, `
OOO
XXXO
XXXO
 XXO
`, `
OOO
XXXO
 XXO
`, `
OOO
XXXO
XXXO
X XO
`, `
 O
 X 
XXXO
XXXO
`, `
OOO
XXXO
 XXO
 XXO
XXXO
`, `
OOO
XXXO
XXXO
XXXO
`, `
OOO
XXXO
X XO
XXXO
`
]);

export default function tryCreateRoom(
    state: DungeonState,
    seed: number,
    availableDoorLocations: Point[],
    foundDoorLocations: Set<Point>
): Room | null {
    const rng = new Rng(seed);
    while (availableDoorLocations.length) {
        const doorLocation = rng.choose(availableDoorLocations);
        
        let i = 0;
        while (i++ < 100) {
            const template = ROOM_TEMPLATES[rng.int(0, ROOM_TEMPLATES.length)];
            const tiles = template.getTiles(doorLocation, rng.next());
            const room = new Room(tiles);
            
            if (state.tryPlaceRoom(room)) {
                state.tryUpdateTile(doorLocation.x, doorLocation.y, tile => tile.setContainsDoor(true));
                room.getNeighbours().forEach(wall => {
                    const wallTile = state.tryGetTile(wall.x, wall.y);
                    if (wallTile && !foundDoorLocations.has(wall)) {
                        foundDoorLocations.add(wall);
                        availableDoorLocations.push(wall);
                    }
                });

                return room;
            }
        }
    }
    
    return null;
}
