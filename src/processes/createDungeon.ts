import DungeonState from '../model/DungeonState';
import tryCreateRoom from './tryCreateRoom';
import Rng from '../model/Rng';
import Thing from '../model/Thing';
import Point from '../model/Point';

function tryCreateDungeon(seed: number): DungeonState | null {
    const rng = new Rng(seed);
    const state = new DungeonState();

    const possibleDoorLocations = new Array<Point>(new Point(0, 1));
    const foundDoorLocations = new Set<Point>(possibleDoorLocations);
    for (let rooms = 0; rooms < 12; rooms++) {
        const room = tryCreateRoom(state, rng.next(), possibleDoorLocations, foundDoorLocations);
        if (!room) {
            return null;
        }

        if (rooms === 0) {
            const player = new Thing({ isPlayer: true });
            state.tryMoveThing(player, rng.select(room.tiles));
        }
    }

    return state;
}

export default function createDungeon(seed: number): DungeonState {
    const rng = new Rng(seed);
    let i = 0;
    while (i < 100) {
        const state = tryCreateDungeon(rng.next());
        if (state) {
            return state;
        }
        i++;
    }
    throw new Error(`Failed to generate dungeon after ${i} attempts.`);
}