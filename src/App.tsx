import logo from './logo.svg';
import './App.css';
import React, { useEffect } from "react";
import { useDispatch } from 'react-redux';
import { updateDungeon } from './store/reducers/gameReducer';
import createDungeon from './processes/createDungeon';
import DungeonRendering from './components/DungeonRendering';

export default function App() {
  const dispatch = useDispatch()

  useEffect(() => {
    const dungeon = createDungeon(1);

    dispatch(updateDungeon(dungeon));

    document.addEventListener("keydown", e => {

      console.log(e);

    });
  });

  return (
    <div className="App">
      <DungeonRendering />
    </div>
  );
}